# Строки
#1)  Напишите скрипт, вычисляющий двумя способами длину строки.
#Первый способ
str = 'Клара украла кораллы'
v = len(str) 
print(v)
#Второй способ
str = 'ky-ky'
print(len(str))
#2) Напишите скрипт, который заменяет произвольный символ/букву в строке на «$».
x = 'С b Днем космонавтики - товарищи'
x = x.replace('b', '$').replace('-','$')
print(x)
#3) Напишите скрипт, который позволяет из строки собрать другую по следующим правилам: новая строка должна состоять из двух первых и двух последних элементов исходной строки. Если длина исходной строки меньше двух, то результатом будет пустая строка.
str_ = input('Введите:')
if len(str_) < 2: 
    print()
else:
    result = str_[:2] + str_[-2:] 
    print(result)
#4) Напишите скрипт, который позволяет инвертировать последователь­ность элементов в строке.
text = 'яро в тиши творя'[::-1]
print(text)
#5) Напишите скрипт, выводящий все элементы строки с их номерами индексов.
w = ('world')
index = 0
while index < len(w):
    print(w[index] + ' - ' + str(index))
    index = index + 1
#6) Поменяйте регистр элементов строки на противоположный.
m = "привет"
print(m.swapcase())
#7) Выведите символ, который встречается в строке чаще, чем остальные.
string = 'Max Pax'
chars = {}
for i in string:
    chars.update({string.count(i): i})
print((max(chars.items())[1]))
# Списки
#8) Вычислите сумму элементов (чисел) в списке двумя разными спосо­бами.
# Первый способ
summ = 0
for i in [1, 2]:
    summ += i
print(summ)
#Второй способ
m = [1, 2]
l = len(m)
s = 0
c = 0
while c < l:
    s = s + m[c]
    c = c + 1
print(s)
#9) Умножьте каждый элемент списка на произвольное число.
my_list = [1, 2]
my_new_list = [i * 2 for i in my_list]
print(my_new_list)
#10) Напишите скрипт для слияния (конкатенации) двух списков тремя различ­ными способами.
#Первый
a = [1, 2]
b = [3, 4]
c  = a + b
print (c)
#Второй
list1 = [1, 2] 
list2 = [3, 4] 
res = [j for i in [list1, list2] for j in i] 
print (str(res))
#Третий
list1 = [1, 2] 
list2 = [3,4]
list1.extend(list2) 
print (list1)
#11) Напишите скрипт, меняющий позициями элементы списка с индек­сами n и n+1
arr = [1,2,3,4]
k = -1 
for i in range(len(arr)//2):
    arr[k], arr[i] = arr[i], arr[k]
    print(arr)
    k -= 1
#12) Напишите скрипт, переводящий список из различного количества числовых целочисленных элементов в одно число. Пример списка: [1, 23, 456], результат: 123456.
x=[ 1, 4,342 ]
print(*x, sep='')
# Словари
#13) Добавьте еще несколько пар «ключ: значение» в следующий словарь: {0: 10, 1: 20}.
d = {
    '0': '10',
    '1': '20',
}
d.update({'2': '30','3': '40'}) 
print(d)
#14) Напишите скрипт, который из трех словарей создаст один новый.
def merge(d1,d2, dict): 
    result = d1 | d2 | d4
    return result 
d1 = {'A' : 'Auf'} 
d2 = {'B' : 'Beer'} 
d4 = {'C' : 'Chips'} 
d3 = merge(d1, d2, d4) 
print(d3)   
#15) Напишите скрипт для удаления элемента словаря.
m = {'a': '1', 'b': '2', 'c': '3'}
poppedItem = m.pop('c')
print(m)
#16) Напишите скрипт, проверяющий, существует ли заданный ключ в словаре.
f= dict(a= 1, b= 3, c= 4)
d = 'a'
if d in f:
    print('Key Found')
else:
    print('Key not found')
# Кортежи
#17) Объявите и инициализируйте кортеж различными способами, после чего распакуйте его.
#№1: 
literal_creation = ('any', 'object')
print(literal_creation)
print(type(literal_creation))
#№2: 
tuple_creation = tuple('any iterable object')
print(tuple_creation)
print(type(tuple_creation))
# Распаковка
n = ('A', 'B', 'C',)
a, b, c = n
print(a)
#18) Конвертируйте список в кортеж, затем добавьте в кортеж новые элементы.
list = [1, 2, 3, 4, 5]
print(list)
print(type(list))
tpl = tuple(list)
print(tpl)
print(type(tpl))
Newtpl = tpl + (6,)
print(Newtpl)
print(type(Newtpl))
#19) Конвертируйте кортеж в словарь.
test_tup1 = ('A', 'B', 'C')
test_tup2 = (1, 2, 3)
print(test_tup1)
print(test_tup2)
res = {test_tup1[i] : test_tup2[i] for i, _ in enumerate(test_tup2)}
print("Dictionary: " + str(res))
#20) Напишите скрипт, подсчитывающий количество элементов типа кортеж в списке.
List = [1.2, 'a', (2,), (3,), (6,)]
Tuple = 0
for i in List:
    if type(i) == tuple :
        Tuple += 1
print(Tuple)
# Множества
#21) Объявите и инициализируйте множество тремя различными способами.
# 1.
nu = {1, 2, 3, 4, 5, 6}  
print(nu)
# 2.
num_set = set([1, 2, 3, 1, 2])  
print(num_set)
# 3. x = set()  
print(type(x))
#22) Удалите элемент из множества.
sets = {0, 1, 2, 3, 4, 5}
sets.remove(5)
print (sets)
#23) Удалите повторяющиеся элементы из списка.
sets = {0, 1, 2, 3, 4, 5, 0, 1, 2, 3, 4, 5}
sets = list(set(sets))
print (sets)
#24) Напишите скрипт, определяющий длину множества двумя различными способами.
# 1.
sets = {0, 1, 2, 3, 4, 5, 0, 1, 2, 3, 4, 5}
sets = list(set(sets))
print (sets)
print(len(sets))
# 2. 
def f (set):
    counter = 0    
    for i in set:
        counter += 1
    return counter
set = {1, 2, 3, 4}
print(f(set))
#25) Напишите скрипт для проверки, входит ли элемент в множество.
sets = {0, 1, 2, 3, 4, 5}
print (sets)
if 6 in sets:
    print("True")
else:
    print("False")